<?php

namespace Torside\LaravelSlovakLocations\Controllers\Index;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Torside\SlovakLocations\Providers\DistrictProvider;

class DistrictsByMunicipalityController extends Controller
{

    /**
     * @param DistrictProvider $provider
     * @param int $municipalityObjectId
     *
     * @return JsonResponse
     */
    public function __invoke(DistrictProvider $provider, int $municipalityObjectId)
    {
        try {

            /** @var array $county */
            $data = $provider->getDistrictsByMunicipality($municipalityObjectId)
                ->toArray();

            return response()->json([
                'code' => Response::HTTP_OK,
                'message' => Response::$statusTexts[Response::HTTP_OK],
                'data' => $data
            ], Response::HTTP_OK);

        } catch (Exception $e) {

            return response()->json([
                'code' => Response::HTTP_NOT_FOUND,
                'message' => Response::$statusTexts[Response::HTTP_NOT_FOUND]
            ], Response::HTTP_NOT_FOUND);

        }
    }

}