<?php

namespace Torside\LaravelSlovakLocations\Controllers\Index;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Torside\SlovakLocations\Providers\StreetProvider;

class StreetsByMunicipalityController extends Controller
{

    /**
     * @param StreetProvider $provider
     *
     * @param int $municipalityObjectId
     *
     * @return JsonResponse
     */
    public function __invoke(StreetProvider $provider, int $municipalityObjectId)
    {
        try {

            /** @var array $data */
            $data = $provider->getStreetsByMunicipality($municipalityObjectId)
                ->toArray();

            return response()->json([
                'code' => Response::HTTP_OK,
                'message' => Response::$statusTexts[Response::HTTP_OK],
                'data' => $data
            ], Response::HTTP_OK);

        } catch (Exception $e) {

            return response()->json([
                'code' => Response::HTTP_NOT_FOUND,
                'message' => Response::$statusTexts[Response::HTTP_NOT_FOUND]
            ], Response::HTTP_NOT_FOUND);

        }
    }

}