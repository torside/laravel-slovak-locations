<?php

use Illuminate\Support\Facades\Route;
use Torside\LaravelSlovakLocations\Controllers\Index\ {
    RegionsController, RegionController, CountiesByRegionController, CountyController, MunicipalitiesByCountyController, MunicipalityController, DistrictsByMunicipalityController, StreetsByMunicipalityController, DistrictController, StreetsByDistrictController, BuildingsByMunicipalityController, BuildingsByDistrictController, StreetController, EntrancesByStreetController, BuildingController, EntrancesByBuildingController, EntranceController
};

use Torside\LaravelSlovakLocations\Controllers\Search\ {
    EntrancesByCoordinatesController
};

Route::name('torside.slovakLocations')->prefix('torside/slovak-locations')->group(function () {

    Route::group(['middleware' => 'api'], function () {

        Route::get('regions', RegionsController::class)->name('regions');

        Route::get('region/{regionObjectId}', RegionController::class)->name('region');
        Route::get('region/{regionObjectId}/counties', CountiesByRegionController::class)->name('countiesByRegion');

        Route::get('county/{countyObjectId}', CountyController::class)->name('county');
        Route::get('county/{countyObjectId}/municipalities', MunicipalitiesByCountyController::class)->name('municipalitiesByCounty');

        Route::get('municipality/{municipalityObjectId}', MunicipalityController::class)->name('municipality');
        Route::get('municipality/{municipalityObjectId}/districts', DistrictsByMunicipalityController::class)->name('districtsByMunicipality');
        Route::get('municipality/{municipalityObjectId}/streets', StreetsByMunicipalityController::class)->name('streetsByMunicipality');
        Route::get('municipality/{municipalityObjectId}/buildings', BuildingsByMunicipalityController::class)->name('buildingsByMunicipality');

        Route::get('district/{districtObjectId}', DistrictController::class)->name('district');
        Route::get('district/{districtObjectId}/streets', StreetsByDistrictController::class)->name('streetsByDistrict');
        Route::get('district/{districtObjectId}/buildings', BuildingsByDistrictController::class)->name('buildingsByDistrict');

        Route::get('street/{streetObjectId}', StreetController::class)->name('street');
        Route::get('street/{streetObjectId}/entrances', EntrancesByStreetController::class)->name('entrancesByStreet');

        Route::get('building/{buildingObjectId}', BuildingController::class)->name('building');
        Route::get('building/{buildingObjectId}/entrances', EntrancesByBuildingController::class)->name('entrancesByBuilding');

        Route::get('entrance/{entranceObjectId}', EntranceController::class)->name('entrance');
        Route::get('search/entrances/coordinates', EntrancesByCoordinatesController::class)->name('searchEntrancesByCoordinates');

    });

});