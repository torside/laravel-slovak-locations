<?php

namespace Torside\LaravelSlovakLocations\Controllers\Index;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Torside\SlovakLocations\Providers\BuildingProvider;

class BuildingController extends Controller
{

    /**
     * @param BuildingProvider $provider
     * @param int $buildingObjectId
     *
     * @return JsonResponse
     */
    public function __invoke(BuildingProvider $provider, int $buildingObjectId)
    {
        try {

            /** @var array $data */
            $data = $provider->getBuildingById($buildingObjectId)
                ->toArray();

            return response()->json([
                'code' => Response::HTTP_OK,
                'message' => Response::$statusTexts[Response::HTTP_OK],
                'data' => $data
            ], Response::HTTP_OK);

        } catch (Exception $e) {

            return response()->json([
                'code' => Response::HTTP_NOT_FOUND,
                'message' => Response::$statusTexts[Response::HTTP_NOT_FOUND]
            ], Response::HTTP_NOT_FOUND);

        }
    }

}