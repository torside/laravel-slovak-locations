<?php

namespace Torside\LaravelSlovakLocations\Controllers\Search;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Torside\SlovakLocations\Entities\GeoPointEntity;
use Torside\SlovakLocations\Providers\EntranceProvider;

class EntrancesByCoordinatesController extends Controller
{

    /**
     * @param EntranceProvider $entranceProvider
     *
     * @return JsonResponse*
     */
    public function __invoke(EntranceProvider $entranceProvider)
    {
        $lat = Input::get('lat');
        $lon = Input::get('lon');
        $size = (int)Input::get('size');
        $distance = Input::get('distance');

        if (empty ($lat) || empty($lon)) {
            return response()->json([
                'code' => Response::HTTP_BAD_REQUEST,
                'message' => Response::$statusTexts[Response::HTTP_BAD_REQUEST]
            ], Response::HTTP_BAD_REQUEST);
        }

        if (empty($size) || (int)$size > 100 || (int)$size < 1) {
            $size = 1;
        }

        if (empty($distance)) {
            $distance = '5m';
        }

        try {
            /** @var GeoPointEntity $coordinates */
            $coordinates = new GeoPointEntity($lat, $lon);

            /** @var array $data */
            $data = $entranceProvider->getEntrancesByCoordinates($coordinates, $size, $distance)
                ->toArray();

            return response()->json([
                'code' => Response::HTTP_OK,
                'message' => Response::$statusTexts[Response::HTTP_OK],
                'data' => $data
            ], Response::HTTP_OK);

        } catch (Exception $e) {

            return response()->json([
                'code' => Response::HTTP_NOT_FOUND,
                'message' => Response::$statusTexts[Response::HTTP_NOT_FOUND]
            ], Response::HTTP_NOT_FOUND);

        }
    }

}