<?php

namespace Torside\LaravelSlovakLocations\Controllers\Index;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Torside\SlovakLocations\Providers\EntranceProvider;

class EntrancesByBuildingController extends Controller
{

    /**
     * @param EntranceProvider $provider
     * @param int $buildingObjectId
     *
     * @return JsonResponse
     */
    public function __invoke(EntranceProvider $provider, int $buildingObjectId)
    {
        try {

            /** @var array $county */
            $data = $provider->getEntrancesByBuilding($buildingObjectId)
                ->toArray();

            return response()->json([
                'code' => Response::HTTP_OK,
                'message' => Response::$statusTexts[Response::HTTP_OK],
                'data' => $data
            ], Response::HTTP_OK);

        } catch (Exception $e) {

            return response()->json([
                'code' => Response::HTTP_NOT_FOUND,
                'message' => Response::$statusTexts[Response::HTTP_NOT_FOUND]
            ], Response::HTTP_NOT_FOUND);

        }
    }

}