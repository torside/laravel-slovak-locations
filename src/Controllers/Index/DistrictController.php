<?php

namespace Torside\LaravelSlovakLocations\Controllers\Index;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Torside\SlovakLocations\Providers\DistrictProvider;
use Torside\SlovakLocations\Providers\StreetProvider;

class DistrictController extends Controller
{

    /**
     * @param DistrictProvider $districtProvider
     * @param StreetProvider $streetProvider
     * @param int $districtObjectId
     *
     * @return JsonResponse
     */
    public function __invoke(
        DistrictProvider $districtProvider,
        StreetProvider $streetProvider,
        int $districtObjectId
    )
    {
        try {

            /** @var array $district */
            $data = $districtProvider->getDistrictById($districtObjectId)
                ->toArray();

            if ($streetProvider->getStreetsByDistrictCount($districtObjectId) > 0) {
                $data['streets'] = $streetProvider->getStreetsByDistrict($districtObjectId);
            }

            return response()->json([
                'code' => Response::HTTP_OK,
                'message' => Response::$statusTexts[Response::HTTP_OK],
                'data' => $data
            ], Response::HTTP_OK);

        } catch (Exception $e) {

            return response()->json([
                'code' => Response::HTTP_NOT_FOUND,
                'message' => Response::$statusTexts[Response::HTTP_NOT_FOUND]
            ], Response::HTTP_NOT_FOUND);

        }
    }

}