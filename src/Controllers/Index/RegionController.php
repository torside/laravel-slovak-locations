<?php

namespace Torside\LaravelSlovakLocations\Controllers\Index;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Torside\SlovakLocations\Providers\RegionProvider;
use Torside\SlovakLocations\Providers\CountyProvider;

class RegionController extends Controller
{

    /**
     * @param RegionProvider $regionProvider
     * @param CountyProvider $countyProvider
     * @param int $regionObjectId
     *
     * @return JsonResponse
     */
    public function __invoke(
        RegionProvider $regionProvider,
        CountyProvider $countyProvider,
        int $regionObjectId
    )
    {
        try {

            /** @var array $data */
            $data = $regionProvider->getRegionById($regionObjectId)
                ->toArray();
            $data['counties'] = $countyProvider->getCountiesByRegion($regionObjectId);

            return response()->json([
                'code' => Response::HTTP_OK,
                'message' => Response::$statusTexts[Response::HTTP_OK],
                'data' => $data
            ], Response::HTTP_OK);

        } catch (Exception $e) {

            return response()->json([
                'code' => Response::HTTP_NOT_FOUND,
                'message' => Response::$statusTexts[Response::HTTP_NOT_FOUND]
            ], Response::HTTP_NOT_FOUND);

        }
    }

}