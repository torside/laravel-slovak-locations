<?php

namespace Torside\LaravelSlovakLocations;

use Illuminate\Contracts\Container\Container;
use Illuminate\Support\ServiceProvider;
use Torside\SlovakLocations\ElasticSearchConnect;
use Torside\SlovakLocations\Providers\LocationProvider;
use Torside\SlovakLocations\Providers\RegionProvider;
use Torside\SlovakLocations\Providers\CountyProvider;
use Torside\SlovakLocations\Providers\MunicipalityProvider;
use Torside\SlovakLocations\Providers\DistrictProvider;
use Torside\SlovakLocations\Providers\StreetProvider;
use Torside\SlovakLocations\Providers\BuildingProvider;
use Torside\SlovakLocations\Providers\EntranceProvider;

class LaravelSlovakLocationsServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ElasticSearchConnect::class, function () {
            /** @var string $url */
            $url = sprintf('%s://%s:%d', env('ES_SCHEME'), env('ES_HOST'), env('ES_PORT'));

            return new ElasticSearchConnect(['base_uri' => $url]);
        });

        $this->app->bind(LocationProvider::class, function (Container $app) {
            return new LocationProvider($app->get(ElasticSearchConnect::class), env('ES_INDEX_VER'));
        });

        $this->app->bind(RegionProvider::class, function (Container $app) {
            return new RegionProvider($app->get(LocationProvider::class));
        });

        $this->app->bind(CountyProvider::class, function (Container $app) {
            return new CountyProvider($app->get(LocationProvider::class));
        });

        $this->app->bind(MunicipalityProvider::class, function (Container $app) {
            return new MunicipalityProvider($app->get(LocationProvider::class));
        });

        $this->app->bind(DistrictProvider::class, function (Container $app) {
            return new DistrictProvider($app->get(LocationProvider::class));
        });

        $this->app->bind(StreetProvider::class, function (Container $app) {
            return new StreetProvider($app->get(LocationProvider::class));
        });

        $this->app->bind(BuildingProvider::class, function (Container $app) {
            return new BuildingProvider($app->get(LocationProvider::class));
        });

        $this->app->bind(EntranceProvider::class, function (Container $app) {
            return new EntranceProvider($app->get(LocationProvider::class));
        });
    }

}
