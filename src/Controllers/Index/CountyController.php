<?php

namespace Torside\LaravelSlovakLocations\Controllers\Index;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Torside\SlovakLocations\Providers\CountyProvider;
use Torside\SlovakLocations\Providers\MunicipalityProvider;

class CountyController extends Controller
{

    /**
     * @param CountyProvider $countyProvider
     * @param MunicipalityProvider $municipalityProvider
     * @param int $countyObjectId
     *
     * @return JsonResponse
     */
    public function __invoke(
        CountyProvider $countyProvider,
        MunicipalityProvider $municipalityProvider,
        int $countyObjectId
    )
    {
        try {

            /** @var array $county */
            $data = $countyProvider->getCountyById($countyObjectId)
                ->toArray();
            $data['municipalities'] = $municipalityProvider->getMunicipalitiesByCounty($countyObjectId);

            return response()->json([
                'code' => Response::HTTP_OK,
                'message' => Response::$statusTexts[Response::HTTP_OK],
                'data' => $data
            ], Response::HTTP_OK);

        } catch (Exception $e) {

            return response()->json([
                'code' => Response::HTTP_NOT_FOUND,
                'message' => Response::$statusTexts[Response::HTTP_NOT_FOUND]
            ], Response::HTTP_NOT_FOUND);

        }
    }

}