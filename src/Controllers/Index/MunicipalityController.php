<?php

namespace Torside\LaravelSlovakLocations\Controllers\Index;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Torside\SlovakLocations\Providers\BuildingProvider;
use Torside\SlovakLocations\Providers\MunicipalityProvider;
use Torside\SlovakLocations\Providers\DistrictProvider;
use Torside\SlovakLocations\Providers\StreetProvider;

class MunicipalityController extends Controller
{

    /**
     * @param MunicipalityProvider $municipalityProvider
     * @param DistrictProvider $districtProvider
     * @param StreetProvider $streetProvider
     * @param BuildingProvider $buildingProvider
     * @param int $municipalityObjectId
     *
     * @return JsonResponse
     */
    public function __invoke(
        MunicipalityProvider $municipalityProvider,
        DistrictProvider $districtProvider,
        StreetProvider $streetProvider,
        BuildingProvider $buildingProvider,
        int $municipalityObjectId
    )
    {
        try {

            /** @var array $response */
            $response = [
                'code' => Response::HTTP_OK,
                'message' => Response::$statusTexts[Response::HTTP_OK]
            ];

            /** @var array $data */
            $data = $municipalityProvider->getMunicipalityById($municipalityObjectId)
                ->toArray();

            if ($districtProvider->getDistrictsByMunicipalityCount($municipalityObjectId) > 0) {
                $data['districts'] = $districtProvider->getDistrictsByMunicipality($municipalityObjectId);
                $response['data'] = $data;
                return response()->json($response, Response::HTTP_OK);
            }

            if ($streetProvider->getStreetsByMunicipalityCount($municipalityObjectId) > 0) {
                $data['streets'] = $streetProvider->getStreetsByMunicipality($municipalityObjectId);
                $response['data'] = $data;
                return response()->json($response, Response::HTTP_OK);
            }

            if ($buildingProvider->getBuildingsByMunicipalityCount($municipalityObjectId) > 0) {
                $data['buildings'] = $buildingProvider->getBuildingsByMunicipality($municipalityObjectId);
                $response['data'] = $data;
                return response()->json($response, Response::HTTP_OK);
            }

            return response()->json($response, Response::HTTP_OK);

        } catch (Exception $e) {

            return response()->json([
                'code' => Response::HTTP_NOT_FOUND,
                'message' => Response::$statusTexts[Response::HTTP_NOT_FOUND]
            ], Response::HTTP_NOT_FOUND);

        }
    }

}